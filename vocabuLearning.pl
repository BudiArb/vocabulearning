:- ['generateKosakata.pl',
    'scramble.pl',
    'hangman.pl'].

% Start Menu (Main Program)
ayomain :-
    write('Selamat datang di VocabuLearning!'), nl,
    pilihKosakata(1, 30),
    nl,write('Silahkan pilih permainan kata berikut (ketik 1/2):'), nl,
    write('1. Hangman'), nl,
    write('2. Scramble Word'), nl,
    read(Game), playGame(Game).

playGame(1) :- !, playhangman(3).
playGame(2) :- !, playscramble(10).
playGame(_) :-
    write('Silahkan pilih 1 atau 2.'), nl,
    read(Game), playGame(Game).