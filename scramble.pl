playscramble(Batas) :-
    kata(ListKata),
    length(ListKata, N),
    Batas =< N,
    !,
    random_permutation(ListKata, ListKataAcak),
    main(ListKataAcak, Batas, 0).

playscramble(_) :-
    write('Jumlah kata pada kosakata kurang dari batas soal.'), nl.

compute(X, Y, 'Benar', Score, ScoreBaru) :- X = Y, !, ScoreBaru is Score + 1.
compute(_, Y, Verdict, Score, Score) :- 
    atomic_list_concat(['Salah | Jawabannya: ', Y], Verdict).

main(_, Sisa, Score) :-
    Sisa = 0, !,
    write('Total Score: '),
    write(Score).

main([Kata|ListKata], Sisa, Score) :-
    Sisanya is Sisa - 1,
    acak(Kata, KataAcak),
    nl,
    write(KataAcak),
    nl,
    read(Guess),
    compute(Guess, Kata, Verdict, Score, ScoreBaru),
    write(Verdict),
    nl,
    write('Score: '),
    write(ScoreBaru),
    nl,
    main(ListKata, Sisanya, ScoreBaru).

acak(Kata, Ret) :-
    atom_chars(Kata, ListKata),
    random_permutation(ListKata, ListKataAcak),
    atomics_to_string(ListKataAcak, Ret).
