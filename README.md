# VocabuLearning

VocabuLearning merupakan permainan kata yang dapat membantu menambah kosakata Bahasa Indonesia Anda. VocabuLearning terdiri dari 2 permainan kata, yaitu Hangman dan Scramble Word.

Dibuat oleh Kelompok FBI:
- R Fausta Anugrah Dianparama - 1906285560
- Budiman Arbenta - 1906285535
- Ilma Alpha Mannix - 1906351045


## Scramble Word

Scramble Word merupakan permainan menyusun kata. Dalam satu kali permaianan, program akan memberikan 10 kata yang urutan huruf nya sudah diacak. Selanjutnya, program akan mengeluarkan kata satu persatu. Lalu, user akan diberikan kesempatan untuk mengurutkan huruf dari kata tersebut menjadi kesatuan kata yang utuh. Jika user berhasil mengurutkan huruf, maka user akan mendapatkan feedback dan poin. Namun, jika user gagal mengurutkan huruf, maka user tetap mendapatkan feedback tetapi poinnya tidak bertambah.

## Hangman

Hangman merupakan permainan tebak kata. Program akan memberikan 1 kata dan 8 nyawa untuk satu kali permainan. Hal yang harus dilakukan oleh user adalah menebak huruf yang terdapat pada kata tersebut. Namun, jika user salah menebak huruf, maka jumlah nyawa yang dimilikinya akan berkurang 1 untuk setiap kesalahan. Jika tidak tersisa nyawa (0), maka permainan akan dihentikan dan user dianggap kalah.
