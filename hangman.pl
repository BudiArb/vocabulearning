playhangman(Batas) :-
    kata(ListKata),
    length(ListKata, N),
    Batas =< N,
    !,
    random_permutation(ListKata, ListKataAcak),
    main(ListKataAcak, Batas, 0, 8).

playhangman(_) :-
    write('Jumlah kata pada kosakata kurang dari batas soal.'), nl.

cetakList([H], _) :-
    write(H).

cetakList([C|T], Pemisah) :-
    write(C),
    write(Pemisah), 
    cetakList(T, Pemisah).

gabung(_, [], [], []).

gabung(Guess, [_|ListSementara], [H|KataList], [H|ListBaru]) :-
    H = Guess,
    !,
    gabung(Guess, ListSementara, KataList, ListBaru).

gabung(Guess, [H|ListSementara], [_|KataList], [H|ListBaru]) :-
    gabung(Guess, ListSementara, KataList, ListBaru).

hangman(KataList, _, Nyawa, Score, Score) :-
	Nyawa = 0,
    cetakGambar(Nyawa),
    !,
    write('Salah | Jawabannya: '), cetakList(KataList,'').

hangman(KataList, ListSementara, _, Score, ScoreBaru) :-
    KataList = ListSementara,
    !,
    write('Betul | Jawabannya: '), cetakList(KataList,''),
    ScoreBaru is Score + 1.

hangman(KataList, ListSementara, Nyawa, Score, ScoreBaru) :-
    write('Nyawa: '), write(Nyawa), nl,
    cetakGambar(Nyawa),
    cetakList(ListSementara, ' '), nl,
    read(Guess),
    cek(Guess, ListSementara, KataList, ListBaru, Nyawa, NyawaBaru),
    hangman(KataList, ListBaru, NyawaBaru, Score, ScoreBaru).

cek(Guess, ListSementara, KataList, ListBaru, Nyawa, Nyawa) :-
    member(Guess, KataList),
    !,
    gabung(Guess, ListSementara, KataList, ListBaru).

cek(_, ListSementara, _, ListSementara, Nyawa, NyawaBaru) :-
    NyawaBaru is Nyawa - 1.

buatList(0, []) :-
    !.
    
buatList(N, ['_'|T]) :-
    NBaru is N - 1,
    buatList(NBaru, T).

main(_, Sisa, Score, _) :-
    Sisa = 0, !,
    write('Total Score: '),
    write(Score).

main([Kata|ListKata], Sisa, Score, Nyawa) :-
    Sisanya is Sisa - 1,
    atom_chars(Kata, KataList),
    length(KataList,N),
    buatList(N, ListKosong),
    hangman(KataList, ListKosong, Nyawa, Score, ScoreBaru),
    nl,
    write('Score: '),
    write(ScoreBaru),
    nl,
    main(ListKata, Sisanya, ScoreBaru, Nyawa).

cetakGambarBaris([]) :-
    nl.

cetakGambarBaris([H|T]) :-
    write(H),nl,
    cetakGambarBaris(T).

cetakGambar(X) :-
    gambar(X, Gam),
    cetakGambarBaris(Gam).

gambar(0, [
'     ________________',
'    |               |',
'    |               |',
'    |               O',
'    |              /|\\',
'    |              / \\',
'    |',
' _______'
]).

gambar(1, [
'     ________________',
'    |',
'    |',
'    |               O',
'    |              /|\\',
'    |              / \\',
'    |',
' _______'
]).

gambar(2, [
'     ________________',
'    |',
'    |',
'    |               O',
'    |              /|\\',
'    |',
'    |',
' _______'
]).

gambar(3, [
'     ________________',
'    |',
'    |',
'    |               O',
'    |               |',
'    |',
'    |',
' _______'
]).

gambar(4, [
'     ________________',
'    |',
'    |',
'    |               O',
'    |',
'    |',
'    |',
' _______'
]).

gambar(5, [
'     ________________',
'    |',
'    |',
'    |',
'    |',
'    |',
'    |',
' _______'
]).

gambar(6, [
'',
'    |',
'    |',
'    |',
'    |',
'    |',
'    |',
' _______'
]).

gambar(7, [
' _______'
]).

gambar(8, []).
