:- dynamic kata/1.
:- dynamic ada/2.

pilihKosakata(MinLetter, MaxLetter) :-
    retractall(kata(_)),
    retractall(ada(_, _)),
    write('Kosakata apa yang ingin anda gunakan?'),nl,
    write(' 1. buah : nama-nama buah'),nl,
    write(' 2. hewan : nama-nama hewan'),nl,
    write(' 3. indonesia : beberapa kata dalam bahasa indonesia'),nl,
    write(' 4. mahasiswa : nama mahasiswa di kelas Pemrograman Logika T.A. 2021/2022 Semester Genap'),nl,
    write(' 5. negara : nama-nama negara'),nl,
    write(' 6. tambahkan kosakata anda sendiri pada folder "kosakata/"'),nl,
    read(FileName),
    atomic_list_concat(['kosakata/', FileName], FileKosakata),
    see(FileKosakata),
    makeListKata(ListKata, MinLetter, MaxLetter),
    seen,
    assert(kata(ListKata)),
    length(ListKata, N),
    write(N), write(' kata berhasil ditambahkan.'), nl.

makeListKata(ListKata, MinLetter, MaxLetter) :-
    read(Kata),
    check(Kata, ListKata, MinLetter, MaxLetter).

check(end_of_file, [], _, _) :- !.

check(Kata, [Kata|ListKata], MinLetter, MaxLetter) :-
    atom_length(Kata, Len),
    Len >= MinLetter,
    Len =< MaxLetter,
    \+checkPerulangan(Kata),
    !,
    makeListKata(ListKata, MinLetter, MaxLetter).

check(_, ListKata, MinLetter, MaxLetter) :-
    makeListKata(ListKata, MinLetter, MaxLetter).

checkPerulangan(Kata) :-
    atom_chars(Kata, ListKata),
    msort(ListKata, SortedHuruf),
    checkAda(Kata, SortedHuruf).

checkAda(Kata, SortedHuruf) :- % Kata dengan komposisi huruf tersebut sudah ada.
    ada(KataX, SortedHuruf),
    !,
    write('Kata "'), write(Kata),
    write('" tidak dipakai karena komposisi hurufnya sama dengan "'), write(KataX),
    write('"'), nl.

checkAda(Kata, SortedHuruf) :- % Komposisi baru (tidak ada yang sama).
    assert(ada(Kata, SortedHuruf)), fail.